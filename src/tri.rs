use std::collections::VecDeque;
use std::fmt;
use pred;

pub type VertexHandle = usize;
pub type TriangleHandle = usize;

#[derive(Debug)]
struct Triangle {
    vertices: [VertexHandle; 3],
    neighbours: [Option<TriangleHandle>; 3] //3 optional neighbours, normally in our structure 3 triangles have 1 None neighbour
}

impl Triangle {
    pub fn new(a: VertexHandle, b: VertexHandle, c: VertexHandle) -> Triangle {
        Triangle {
            vertices: [a, b, c],
            neighbours: [None, None, None]
        }
    }

    fn print(&self, tds: &TDS) 
    // as a TDS instance is needed, I can not print the triangle with implementing the fmt::Display trait
    // this could work with an instance of the TDS, and make a method on that struct
    {
        print!("POLYGON(({:.3} {:.3}, {:.3} {:.3}, {:.3} {:.3}, {:.3} {:.3}));{:?};{:?};{:?}\n",
            tds.vertices[self.vertices[0]].x, tds.vertices[self.vertices[0]].y,
            tds.vertices[self.vertices[1]].x, tds.vertices[self.vertices[1]].y,
            tds.vertices[self.vertices[2]].x, tds.vertices[self.vertices[2]].y,
            tds.vertices[self.vertices[0]].x, tds.vertices[self.vertices[0]].y,
            self.neighbours[0], self.neighbours[1], self.neighbours[2])
    }
}

// look, no access to vertices of TDS
impl fmt::Display for Triangle {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({:?})", self.vertices)
    }
}

#[inline]
fn ccw(i: usize) -> usize {
    (i + 1) % 3
}

#[inline]
fn apex(side: usize) -> usize {
    // Given a side, give the corresponding apex index of the triangle
    side % 3
}

#[inline]
fn orig(side: usize) -> usize {
    // Given a side, give the origin index of the triangle
    (side + 1) % 3
}

#[inline]
fn dest(side: usize) -> usize {
    // Given a side, give the destination index of the triangle
    if side == 0 {
        2
    } else {
        (side - 1) % 3
    }
}

#[derive(Debug)]
struct Vertex {
    x: f64,
    y: f64
}

impl Vertex {
    pub fn new(x: f64, y: f64) -> Vertex {
        Vertex { x: x, y: y }
    }
}

#[derive(Debug)]
pub struct TDS {
    vertices: Vec<Vertex>,
    triangles: Vec<Triangle>,
}

impl TDS {

    pub fn new() -> TDS {
        TDS {
            vertices: Vec::new(),
            triangles: Vec::new()
        }
    }

    pub fn init(&mut self, bbox: [(f64, f64); 2]) -> () {
        // based on aabb (around points) make large triangle
        let xmin = bbox[0].0;
        let ymin = bbox[0].1;
        let xmax = bbox[1].0;
        let ymax = bbox[1].1;
        // take longest side
        let mut width = xmax - xmin;
        let height = ymax - ymin;
        if width < height {
            width = height;
        }
        // x
        let left = xmin - 50.0 * width;
        let mid = 0.5 * xmin + 0.5 * xmax;
        let right = xmax + 50.0 * width;
        // y
        let bottom = ymin - 40.0 * width;
        let top = ymax + 60.0 * width;
        // make 3 vertices
        let va = self.insert_vertex(left, bottom);
        let vb = self.insert_vertex(right, bottom);
        let vc = self.insert_vertex(mid, top);
        // the large triangle that encloses the aabb
        self.insert_triangle(va, vb, vc);
    }

    fn visibility_walk(&self, ini: TriangleHandle, pt:(f64, f64)) -> TriangleHandle {
        let mut handle = ini;
        let mut t = &self.triangles[handle];
        'outer: for _ in 0..self.triangles.len() {
            for e in 0..3 {
                let i = ccw(e); 
                let j = ccw(e+1);
                let pt_at_right = pred::orient2d(
                    [self.vertices[t.vertices[i]].x, self.vertices[t.vertices[i]].y],
                    [self.vertices[t.vertices[j]].x, self.vertices[t.vertices[j]].y], 
                    [pt.0, pt.1]
                ) < 0.0;
                if pt_at_right {
                    handle = match t.neighbours[e] {
                        Some(x) => (x),
                        _ => panic!("Neighbour is None and point is at right side of edge!")
                    };
                    t = &self.triangles[handle];
                    continue 'outer;
                }
            }
            return handle;
        }
        panic!("Exhausted iteration in walk: All triangles seen!");
    }

    pub fn add_vertex(&mut self, pt:(f64, f64), ini:Option<TriangleHandle>) -> (TriangleHandle) {
        // find the triangle that contains pt
        let ta = self.visibility_walk(match ini { Some(ini) => ini, _ => 0}, pt);
        // add a new vertex to the structure
        let vh = self.insert_vertex(pt.0, pt.1);
        // insert the vertex into the found triangle and 
        // add two new triangles adjacent to it
        let (va, vb, vc, na, nb, nc) = self.insert_vertex_in_triangle(vh, ta);
        let tb = self.insert_triangle(vb, vc, vh);
        let tc = self.insert_triangle(vc, va, vh);
        // link internal triangles (happens 3*2 = 6 times) -- triangle a, b, c
        self.link_triangle(ta, tb);
        self.link_triangle(tb, ta);
        self.link_triangle(tb, tc);
        self.link_triangle(tc, tb);
        self.link_triangle(ta, tc);
        self.link_triangle(tc, ta);
        // link outside triangles (happens 3*2 = 6 times)  -- neighbour a, b, c
        // put triangle handles (the 3 triangles + their neighbours) both in an array
        let tris = [ta, tb, tc];
        let ngbs = [nc, na, nb];
        // link up the triangles and the neighbours around
        for (t, n) in tris.iter().zip(ngbs.iter())
        {
            let t = *t;
            match *n {
                Some(n) => {
                    self.link_triangle(t, n);
                    self.link_triangle(n, t);
                }
                None => {
                }
            }
        }
        // enforce Delaunay property
        let queue = &mut VecDeque::new(); //Vec::new();
        queue.push_back((ta, 2));
        queue.push_back((tb, 2));
        queue.push_back((tc, 2));
        self.delaunay(queue);
        // give back the triangle handle into which the point was added
        // (this trianglehandle can subsequently be used for point location)
        ta
    }

    pub fn is_consistent(&self) -> bool {
        for (th0, tri0) in self.triangles.iter().enumerate() {
            for nh in &tri0.neighbours {
                match *nh {
                    Some(th1) => {
                        let tri1 = &self.triangles[th1];
                        let side1 = self.common_side(th1, th0);
                        match tri1.neighbours[side1] {
                            Some(x) => {assert!(x == th0);},
                            None => {}
                        };
                        let orig = &self.vertices[tri0.vertices[0]];
                        let dest = &self.vertices[tri0.vertices[1]];
                        let apex = &self.vertices[tri0.vertices[2]];
                        let othr = &self.vertices[tri1.vertices[side1]];
                        let inside = pred::incircle(
                            [orig.x, orig.y], [dest.x, dest.y], [apex.x, apex.y],
                            [othr.x, othr.y]); 
                        // positive incircle => point inside (should not happen for Delaunay)
                        if inside > 0.0 {
                            self.print_triangles();
                            println!("POINT({} {})", orig.x, orig.y);
                            println!("POINT({} {})", dest.x, dest.y);
                            println!("POINT({} {})", apex.x, apex.y);

                            println!("POINT({} {})", othr.x, othr.y);
                            println!("Failing consistency ({}) check at triangles: {} and {}", inside, th0, th1);
                            assert!(false);
                        }
                    },
                    None => {}
                }
            }
        }
        true
    }

    fn delaunay(&mut self, queue: &mut VecDeque<(TriangleHandle, usize)>) -> () {
        while queue.len() > 0 {
            let (th0, side0) = match queue.pop_front() {
                Some(x) => (x.0, x.1),
                _ => panic!("Popping from empty queue...")
            };
            if !self.is_delaunay(th0, side0) {
                // flip the triangle th0 at side0
                // gets the trianglehandle that is also flipped
                let th1 = self.flip(th0, side0);
                // here we schedule the neighbours after the flip
                // for checking their delaunay property
                queue.push_back((th0, 0));
                queue.push_back((th0, 2));
                queue.push_back((th1, 0));
                queue.push_back((th1, 2));
            }
        }
    }

    fn is_delaunay(&self, th0: TriangleHandle, side0: usize) -> bool {
        let tri0 = &self.triangles[th0];
        // return true or false based on whether the neighbour is there...
        // and whether this neighbour has a point inside the circle
        match tri0.neighbours[side0] {
            Some(th1) => {
                let tri1 = &self.triangles[th1];
                let side1 = self.common_side(th1, th0);
                // get the 3 vertices of the triangle and
                // the vertex on the opposite side of the given edge
                let orig = &self.vertices[tri0.vertices[0]];
                let dest = &self.vertices[tri0.vertices[1]];
                let apex = &self.vertices[tri0.vertices[2]];
                let othr = &self.vertices[tri1.vertices[side1]];
                let outside = pred::incircle(
                    [orig.x, orig.y], [dest.x, dest.y], [apex.x, apex.y],
                    [othr.x, othr.y]) < 0.0;
                outside
            }
            None => true // if we do not have a neighbour here, we say it is delaunay
        }
    }

    fn flip(&mut self, th0: TriangleHandle, side0: usize) -> (TriangleHandle) {
        // returns the trianglehandle of the triangle that was neighbouring th0
        // and was used inside the flipping operation
        let (th1, va, vb, vc, vd, ab, bc, cd, da) = self.flip_neighbourhood(th0, side0);
        self.update_triangle(th0, va, vb, vc, bc, Some(th1), ab);
        self.update_triangle(th1, vc, vd, va, da, Some(th0), cd);
        // link up the neighbours around
        let tris = [th0,       th0, th0, th1,       th1, th1];
        let ngbs = [Some(th1), ab,  bc,  Some(th0), cd,  da];
        for (t, n) in tris.iter().zip(ngbs.iter())
        {
            let t = *t;
            match *n {
                Some(n) => {
                    self.link_triangle(n, t);
                }
                None => {
                }
            }
        }
        th1
    }

    fn flip_neighbourhood(&self, th0: TriangleHandle, side0: usize) -> 
        (TriangleHandle,
         VertexHandle, VertexHandle, VertexHandle, VertexHandle,
         Option<TriangleHandle>, Option<TriangleHandle>, Option<TriangleHandle>, Option<TriangleHandle>) {
        // find vertex indices and neighbour indices around quad
        let tri0 = &self.triangles[th0];
        match tri0.neighbours[side0] {
            Some(th1) => {
                let tri1 = &self.triangles[th1];
                let side1 = self.common_side(th1, th0);
                // find what is around
                let apex0 = apex(side0);
                let orig0 = orig(side0);
                let dest0 = dest(side0);
                let apex1 = apex(side1);
                let orig1 = orig(side1);
                let dest1 = dest(side1);
                assert!(tri0.vertices[orig0] == tri1.vertices[dest1]);
                assert!(tri0.vertices[dest0] == tri1.vertices[orig1]);
                // vertices forming the quadrilateral around the edge that needs flipping
                let vha = tri0.vertices[apex0];
                let vhb = tri0.vertices[orig0];
                let vhc = tri1.vertices[apex1];
                let vhd = tri0.vertices[dest0];
                // neighbours around quadrilateral plus their sides
                let ab = tri0.neighbours[dest0];
                let bc = tri1.neighbours[orig1];
                let cd = tri1.neighbours[dest1];
                let da = tri0.neighbours[orig0];
                (th1, vha, vhb, vhc, vhd, ab, bc, cd, da)
            },
            None => panic!("flipping triangle at side with no neighbour")
        }
    }

    fn update_triangle(&mut self, th0: TriangleHandle,
        v0: VertexHandle, v1: VertexHandle, v2: VertexHandle,
        n0: Option<TriangleHandle>, n1: Option<TriangleHandle>, n2: Option<TriangleHandle>) -> () {
        let t = &mut self.triangles[th0];
        t.vertices[0] = v0;
        t.vertices[1] = v1;
        t.vertices[2] = v2;
        t.neighbours[0] = n0;
        t.neighbours[1] = n1;
        t.neighbours[2] = n2;
    }

    pub fn insert_triangle(&mut self, a: VertexHandle, b: VertexHandle, c: VertexHandle) -> TriangleHandle {
        let id = self.triangles.len();
        self.triangles.push(Triangle::new(a, b, c));
        id
    }

    pub fn link_triangle(&mut self, t0: TriangleHandle, t1: TriangleHandle) {
        // find common side
        let s0 = self.common_side(t0, t1);
        // update tri0 its neighbour
        let tri0 = &mut self.triangles[t0];
        tri0.neighbours[s0] = Some(t1);
    }

    #[inline]
    fn common_side(&self, t0: TriangleHandle, t1: TriangleHandle) -> usize {
        // find common side between triangle with handle t0 and t1
        let tri0 = &self.triangles[t0];
        let tri1 = &self.triangles[t1];
        let mut side = 3; // initialize to value that side can not be
        'outer: for t0orig in 0..3 {
            for t1orig in 0..3 {
                let t0apex = ccw(t0orig);
                let t1apex = ccw(t1orig);
                if tri0.vertices[t0orig] == tri1.vertices[t1apex] && 
                    tri1.vertices[t1orig] == tri0.vertices[t0apex] {
                    side = ccw(t0apex);
                    break 'outer;
                }
            }
        }
        if side == 3 {
            panic!("Triangles {} and {} not sharing edge!", t0, t1)
        }
        side
    }

    fn insert_vertex_in_triangle(&mut self, v: VertexHandle, t: TriangleHandle) -> (VertexHandle, VertexHandle, VertexHandle, Option<TriangleHandle>, Option<TriangleHandle>, Option<TriangleHandle>) {
        let tri = &mut self.triangles[t];
        // get old vertex handles
        let va = tri.vertices[0];
        let vb = tri.vertices[1];
        let vc = tri.vertices[2];

        // get old neighbour handles
        let ta = tri.neighbours[0];
        let tb = tri.neighbours[1];
        let tc = tri.neighbours[2];

        // update tip of triangle to make new vertex handle
        tri.vertices[2] = v;
        // return old vertices + neighbours
        (va, vb, vc, ta, tb, tc)
    }

    fn insert_vertex(&mut self, x: f64, y: f64) -> VertexHandle {
        self.vertices.push(Vertex::new(x, y));
        self.vertices.len() - 1
    }

    pub fn print_triangles(&self) {
        for (i, t) in self.triangles.iter().enumerate() {
            print!("{};", i);
            t.print(self);
        }
    }

    pub fn print_vertices(&self) {
        print!("{:?}", self.vertices);
    }
}
