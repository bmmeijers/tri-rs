//! This is the main part of the program

mod tri;
mod pred;
mod reader;

fn main() {
    // read points
    let mut points = &mut reader::read_points("/home/martijn/Documents/work/2017_learning_rust/tri/dlnyrd/points");
    // find their aabb
    let envelope = reader::bbox(points);
    // make an empty triangulation data structure
    let mut tri = tri::TDS::new();
    // initialize big triangle
    tri.init(envelope);
    // pre-process points:
    // annotate each point with their parent point in kdtree depth-first hierarchy
    let sorted = reader::kdsort(points, envelope);
    // incrementally add points, 1 by 1
    // use the order for inserting points as found by kdsort
    // we record the triangle that belongs to an already inserted point
    // to bootstrap the visibility walk from that particular triangle
    let mut triangleh:Vec<tri::TriangleHandle> = Vec::new();
    for item in sorted {
        // see if we have a parent point
        match item.1 {
            // if so, use associated triangle to use for starting walk
            Some(parent) => {
                let mut ini = triangleh[parent];
                ini = tri.add_vertex(item.0, Some(ini));
                triangleh.push(ini);
            },
            // if not, let the triangulation decide where to start walk from
            _ => {
                let ini = tri.add_vertex(item.0, None);
                triangleh.push(ini);
            }
        };
    }
    // print all triangles that were constructed
    tri.print_triangles();
    // post-condition: all triangles are Delaunay
    assert!(tri.is_consistent() == true);
}
