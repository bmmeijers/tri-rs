use std::io::BufRead;
use std::io::BufReader;
//use std::io::prelude::*;
use std::fs::File;
use std::f64;
use std::cmp::Ordering::Less;

/// Reads points from a file
///
/// # Args
///
/// - `filenm`: a path including a filename
pub fn read_points(filenm: &str) -> Vec<(f64, f64)> {
    let f = File::open(filenm).expect("File not there");
    let f = BufReader::new(f);

    let pairs: Vec<_> = f.lines().map(|line| {
        let line = line.unwrap();

        let mut words = line.split_whitespace();
        // This will only be None if it was an empty line.
        // Use .filter_map() and return None if you want to skip those.
        let x = words.next().expect("Unexpected empty line!");
        let y = words.next().expect("Expected second number on line!");
        (
            x.parse::<f64>().ok().expect("Expected float in first column!"), 
            y.parse::<f64>().ok().expect("Expected float in second column!")
        )
    }).collect();
    pairs
}

pub fn bbox(points: &Vec<(f64, f64)>)  -> [(f64, f64); 2] {
    let mut xmin = f64::MAX;
    let mut ymin = f64::MAX;
    let mut xmax = f64::MIN;
    let mut ymax = f64::MIN;

    for pt in points.iter() {
        if pt.0 < xmin {
            xmin = pt.0;
        }
        if pt.0 > xmax {
            xmax = pt.0;
        }
        if pt.0 < ymin {
            ymin = pt.1;
        }
        if pt.0 > ymax {
            ymax = pt.1;
        }
    }
    let aabb = [(xmin, ymin), (xmax, ymax)];
    aabb
}

pub fn largest_axis(aabb: &[(f64, f64); 2])  -> u8 {
    let dx = aabb[1].0 - aabb[0].0;
    let dy = aabb[1].1 - aabb[0].1;
    match dx > dy {
        true => 0,
        false => 1
    }
}

pub fn kdsort(points: &mut Vec<(f64, f64)>, aabb: [(f64, f64); 2]) -> Vec<((f64, f64), Option<usize>)> {
    // low, high, parent, aabb
    let mut result:Vec<((f64, f64), Option<usize>)> = Vec::with_capacity(points.len());
    let mut stack:Vec<(usize, usize, Option<usize>, [(f64,f64); 2])> = Vec::with_capacity(points.len() / 2);
    stack.push((0, points.len(), None, aabb));
    while stack.len() > 0 {
        let (l, u, parent, aabb) = match stack.pop() {
            Some(x) => (x.0, x.1, x.2, x.3),
            _ => panic!("Popping from empty stack")
        };
        // sort points on the axis that is the largest
        let axis = largest_axis(&aabb);
        match axis {
            0 => points[l..u].sort_by(|a, b| (&a.0).partial_cmp(&b.0).unwrap_or(Less)),
            1 => points[l..u].sort_by(|a, b| (&a.1).partial_cmp(&b.1).unwrap_or(Less)),
            _ => panic!("Encountered unknown axis for splitting")
        };
        // split points in two sets
        // adding the pivot (the point in the middle) to the result
        let halfway = (l + u) / 2;  // rounds down due to being integer
        let pivot = points[halfway];
        result.push(((pivot), parent));
        // boundaries of the boxes around the two halves
        let (xmid, ymid) = pivot;
        let (left, bottom) = aabb[0];
        let (right, top) = aabb[1];
        // stack right half
        if (halfway + 1) < u  {
            let half_aabb = match axis {
                0 => [(xmid, bottom), (right, top)],
                1 => [(left, ymid), (right, top)],
                _ => panic!("Encountered unknown axis for splitting")
            };
            stack.push((halfway+1, u, Some(result.len()-1), half_aabb));
        }
        // stack left half
        if halfway > l {
            let half_aabb = match axis {
                0 => [(left, bottom), (xmid, top)],
                1 => [(left, bottom), (right, ymid)],
                _ => panic!("Encountered unknown axis for splitting")
            };
            stack.push((l, halfway, Some(result.len()-1), half_aabb));
        }
    }
    result
}

